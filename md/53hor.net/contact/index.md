---
pagetitle: 53hornet - Home
---
<article>

### Contact Us

#### Monitored by Spam Police

# Intro
Note on requests for pictures/videos: If you would like any pictures or close-up videos of the finished car, tell me what you would like to see and I'll try and send you images of how we did things or how things work. If you want me to take a video of the car (its functions, the restoration, parts close-ups, etc.) then I'll post the response on [YouTube](https://youtube.com/my53hornet).

# Email

Use the email link below to get in touch with any questions you may have. Include your email address so that I can get back to you. I will happily answer any questions I can. If I can't, I'll try to point you to someone in the HET club who will have the answer.

[atc@53hor.net](mailto:atc@53hor.net)

# Reddit

I am also reachable on Reddit at the '53hornet' handle.

[u/53hornet](https://www.reddit.com/user/53hornet)

</article>
