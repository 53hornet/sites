---
pagetitle: 53hornet - Gallery
---
<article>

### Gallery

#### Fabulous eye candy

# 2017

## Dad's Retirement

![[Open Dad's Retirement album](https://nextcloud.53hor.net/index.php/apps/gallery/s/76iowZy9tmkWDDo#)](easter-2017.jpg)

## Easter

![[Open Easter 2017 album](https://nextcloud.53hor.net/index.php/apps/gallery/s/76iowZy9tmkWDDo#)](easter-2017.jpg)

## Independence Day

![[Open Independence Day 2017 album](https://nextcloud.53hor.net/index.php/apps/gallery/s/76iowZy9tmkWDDo#)](easter-2017.jpg)

## 'Wheezy'

![[Open Wheezy's album](https://nextcloud.53hor.net/index.php/apps/gallery/s/76iowZy9tmkWDDo#)](easter-2017.jpg)

## Put-in-Bay

![[Open Put-in-Bay album](https://nextcloud.53hor.net/index.php/apps/gallery/s/76iowZy9tmkWDDo#)](easter-2017.jpg)

## Pontiac, Michigan

![[Open Pontiac, MI album](https://nextcloud.53hor.net/index.php/apps/gallery/s/76iowZy9tmkWDDo#)](easter-2017.jpg)

# 2018

</article>
