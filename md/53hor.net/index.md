---
pagetitle: 53hornet - Home
---
<article>

### Welcome to 53hornet

#### A site for everything about my 1953 Hudson Hornet

# Intro

This site is dedicated to everything about my 1953 Hudson Hornet. It serves as a digital collection documenting all of our adventures on the road and it is the hub from which my YouTube channel and Twitter feed draw. Feel free to explore this site for fun stories, photos, and videos of our Hudson! Dad and I are members of the Hudson Essex Terraplane Club and are actively trying to make it easier to find knowledge and parts for Hudson restorations. Our primary goal is to create an aftermarket and reproduction parts database for Hudson automobiles. Check out Parts Database to learn more.

# Say Hi to 'Ole Blue'

This is Ole Blue. She's a 1953 Hudson Hornet four-door sedan. She has a 308ci flathead straight-six engine and a four-speed 'Dual-Range' Hydramatic transmission made by General Motors. Blue is the reason for this site, as are all of the experiences we had finding parts for her, fixing her up, and taking her on adventures big and small.

!["Ole Blue - Buttercups"](home-1.jpg)

# Using This Site

On top of this page is the navigation bar, which will take you to every page of this site including our parts database, our collection of photos and images, and a page where you can contact us if you have any questions. On the left is a per-page table of contents. Every page you're on will have one of these to make navigation within a page quicker. 

</article>
