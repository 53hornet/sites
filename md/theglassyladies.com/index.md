---
pagetitle: "The Glassy Ladies"
author:
- Elizabeth Carpenter
- Adam Carpenter
keywords: [liz, elizabeth, carpenter, pat, patricia, potter, glass, art, stained, artisans, artists]

---
<article>

# The "Glassy Ladies" Artisans

## Elizabeth Carpenter & Patricia Potter

### Glass with Class!

Our site is still under construction but in the meantime you can check out our artwork [here](https://nextcloud.53hor.net/index.php/apps/gallery/s/AWFkGTCoDKwKF4r#) or find us on Facebook [here](https://www.facebook.com/glassyladiesart/?tn-str=k*F).

</article>
