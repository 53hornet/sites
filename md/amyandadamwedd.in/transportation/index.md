--- 
pagetitle: Transportation 
--- 
<article>

# Transportation

## Ways to get around the island

### Harriet’s House Golf Cart Rentals

Contact Tiffany or Tara at [(419) 341-2191](tel: 419-341-2191). The rates are
$55/day 9AM-6PM for a four-passenger cart ($20 overnight fee\*). _This fee will
be waived if you state you are with the Squire/Carpenter wedding._
<br>\*Overnight carts are due back at 11AM. 

### Golf Cart Depot 

Contact Rich at [(419) 779-5147](tel: 419-779-5147). The going rate for a
four-passenger cart is $70/day 9AM-6PM, $90 overnight, $170 for two nights, and
$240 for three nights\*. The going rate for a six-passenger cart is $95/day
9AM-6PM, $115 overnight, $205 for two nights, and $280 for three nights\*.
_State you are with the Squire-Carpenter wedding to receive a 15% discount.
This does not apply if booked online._ <br>\*Overnight carts are due back at
11AM. 

</article>
