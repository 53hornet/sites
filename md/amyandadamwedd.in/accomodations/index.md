---
pagetitle: Accomodations
---
<article>

# Places to Stay

## Rest and relaxation

For your comfort and convenience we have a hold on several lodging
possibilities. As a popular summer resort area the
rooms book quickly before the summer months. We have worked with several
owners to hold rooms and each owner has a specific deadline date for
reservations. Please note that all rooms require at least a 2 night stay. We recommend
booking Thursday, August 8^th^ and Friday, August 9^th^. 
Please reference the Squire/Carpenter wedding when calling to make your
reservation.
Crew's Nest accommodations are on-site wedding locations. Any locations marked Put-in-Bay are physically on the island. All other locations will require a ferry ride to reach the island. 

If you have any questions about finding accommodations you can contact Amy's mom, *Dianne Squire*,
via email at [terrysquir@aol.com](mailto:terrysquir@aol.com) or via telephone at [330-697-9632](tel:330-697-9632)
and she will be happy to help.


### Winery Carriage House - The Crew's Nest, Put-in-Bay, OH

*Reservations must be made by April 1^st^, 2019.*
Two bedrooms, each featuring a queen size bed; two pull out sofas in the
common room, can accommodate up to eight people, including children.
Amenities include a fully equipped kitchen, WiFi, cable TV, and air conditioning.
[Details on website](http://www.thecrewsnest.com).
If interested, call or email *Dianne Squire* (see above).

### Doller Mansion Suite - The Crew's Nest, Put-in-Bay, OH

*Reservations must be made by April 1^st^, 2019.*
Both rooms of the suite are located on the second floor of the mansion.
The master bedroom overlooks the bay, while the garden bedroom overlooks
the gazebo. You can choose to book one or both rooms for your stay. Each
room has a queen bed and sleeps 2 people. Amenities include a shared
bathroom in the hallway - book for a family or another couple you don't
mind sharing a bathroom with!
[Details on website](http://www.thecrewsnest.com).
If interested, call or email *Dianne Squire* (see above).

### Ahoy Inn Villas - Put-in-Bay, OH

*Reservations must be made by February 1^st^, 2019.*
2 Adults per villa, no children. 9 villas available. Breakfast not included at guest house.
[Details on website](http://www.ahoypib.com).
If interested, call *Simone* for reservations at [419-341-3814](tel:419-341-3814).

### Arbor Inn Bed & Breakfast - Put-in-Bay, OH

*Reservations must be made by April 1^st^, 2019.*
1 or possibly 2 rooms are still available.
[Details on website](http://www.arborinnpib.com).
If interested, call or email *Dianne Squire* (see above).

### English Pines Bed & Breakfast - Put-in-Bay, OH

*Reservations must be made by February 15^th^, 2019.*
children are welcome weekdays. Pets are not permitted. 7 rooms are available. Breakfast is included.
[Details on website](http://www.englishpines.com).
Call *Liz or Doug Knauer* for reservations at [419-285-2521](tel: 419-285-2521).

### Victory Station Hotel - Put-in-Bay, OH

*Reservations must be made by March 1^st^, 2019.*
8 double rooms available - sleeps 4. No one under 21 allowed.
4 single rooms available - sleeps 2. No one under 21 allowed.
Breakfast not included.
[Details on website](http://www.putinbayvictorystation.com).
Call [419-285-0120](tel: 419-285-0120) for reservations.

### Bayshore Resort - Put-in-Bay, OH

*Reservations must be made by February 1^st^, 2019.*
Recommended for families with children. 10 double queen bed rooms blocked. Restaurants and pools on premises. Very family friendly.
Note that Friday, August 9^th^ must be booked in tandem with Saturday, August 10^th^. Thursday, August 8^th^ may be added on but may not be included in the 2 night minimum.
[Details on website](http://www.shoresandislands.com).
Call *Dawn* for reservations at [419-285-3931](tel: 419-285-3931).

### Country Inn Suites - 3760 East State Road, Port Clinton, OH 

*Reservations must be made by July 9^th^, 2019.*
10 rooms blocked. Free, hot breakfast, indoor pool, microwave, and refrigerator in all
rooms. Cedar Point amusement park 25 minutes from hotel. Rooms are currently blocked for Thursday, August 8^th^ and Friday, August 9^th^. If you
would like to stay longer we recommend you book as soon as possible.
Recommended for families with children.
[Details on website](http://www.countryinns.com).
Call *Danielle* for reservations at [419-732-2434](tel: 419-732-2434) or the hotel at [800-447-7011](tel: 800-447-7011).

### Other Options!

We recommend the above locations for a nice family experience. Several
other options are available on the island and you are welcome to explore
anything else you like. Be aware and advised that some of the other island
locations are frequented by weekend partiers!

</article>
