---
pagetitle: "Welcome to our Wedding!"
author:
- Adam Carpenter
- Amy Squire
keywords: [amy, adam, carpenter, squire, wedding, 2019, put-in, bay, ohio, oh, lake, erie]

---
<article>

<figure class="banner"><img src="pib.png" alt="Journey to the island with us"></figure>

# On August 9th, 2019,

## Amy and Adam are tying the knot! Celebrate with us at The Crew's Nest, Put-in-Bay, Ohio

![She'll kiss the groom and he'll kiss the bride](bg.jpg)

Greetings from the bride- and groom-to-be! If you received something from us in the mail then you are cordially invited to witness our union in marriage. Just kidding; it's a casual occasion! We can't wait to see all of our friends and family on the most special day of our lives. The wedding is taking place in a very special place to us: it's a destination wedding to Put-in-Bay, an island on Lake Erie.
**Don't forget to check back here for updates as the day draws near! We'll be posting more information about transportation recommendations, a registry, and information specific to our wedding party members!**


### RSVP Here!

If you're technologically savvy (you made it here!) then you may RSVP through the top of the page [or at this link](/rsvp).

### The Wedding Venue

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2981.1060159030526!2d-82.82247784942254!3d41.65345137913883!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x883a56802d23c2a5%3A0x5c7b2e7da24a6651!2sCrew&#39;s+Nest!5e0!3m2!1sen!2sus!4v1538340726028" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

### Morning-After Brunch

Don't skip town just yet! Dianne and Terry Squire invite you to a morning-after brunch honoring the new Mr. and Mrs. Carpenter. It will take place on August 10^th^ 9:00-11:00AM at The Crew's Nest. If you are interested please call Dianne at [330-697-9632](tel: 330-697-9632) before August 9^th^. [Click here for the invitation](invite.pdf).

### Photo Gallery & Chat Room

You can reach the photo gallery through the top of the page. Here you and everyone else in attendance may share photos in one big gallery for everyone to see! Remember to snap lots of pictures! There's also a link to a private chat room we've built. Feel free to ask us questions about the wedding or send us well-wishes there!

### What on Earth is Put-in-Bay?

Located in the shallow western end of Lake Erie is a group of 20 or more islands. One of these, Put-in-Bay or South Bass, served as a base of operations for Oliver Hazard Perry. It was from the harbor called Put-in-Bay that Perry sailed to defeat the British fleet under Robert H. Barclay during the war of 1812. The American victory in the battle of Lake Erie gave the country and the United States Navy a memorable slogan of positive accomplishments, "We have met the enemy and they are ours..." Today there stands at Put-in-Bay a beautiful Greek Doric column, the Perry's Victory and International Peace Memorial. This 352-foot granite shaft commemorates not only a naval battle but a peace which has lasted for more than 150 years. The 3,987-mile boundary between the United States and Canada is the longest unguarded international frontier in the world.

The brigs, ships, and sloops with their long guns and cannonades are gone. Their place has been taken by yachts and sailboats. Many captains of these pleasure craft plot a course for Put-in-Bay seeking relaxation from the tensions of the city. Others -- yacht-less landlubbers -- board the ferryboats or airplanes for their trip to an island in Ohio's Lake Erie vacation-land. Urban and rural tourists have been coming to Put-in-Bay for over 100 years. From the top of the Perry Memorial, the visitor can observe the site of the Battle of Lake Erie. He can also probe the depths of the caves, bicycle around the island, or sip locally produced wine or grape juice.

The earliest visitors were the American Indians. Many Indian arrowheads, stone axes, and other implements of blue and white flints were turned up during construction. Indians visited Put-in-Bay when ice conditions allowed the crossing to hunt raccoons and other animals.

The French explorer and fur trader Louis Jolliet was the first white man to travel on the lake. An unidentified group of explorers sailed among the islands in July of 1784. They made charts of the islands, naming one of them Pudding Bay because the shape of the harbor (or Put-in-Bay) resembled a pudding bag. Other log books referred to the harbor as Puden Bay. The Lake Erie Islands were included in the tract of land claimed by Connecticut and which is known as the Western Reserve. The earliest white inhabitants known to have occupied the Islands were the French.

Seth Done brought a number of laborers who cleared over 100 acres of land and planted wheat in the summer and fall of 1811. He also imported 400 sheep and 150 hogs to graze on the acorn and hickory nuts which were abundant on the island. The first effort to settle on Put-in-Bay ended with the coming of the war of 1812. The workers were busy threshing grain when British soldiers drove them off in fall of 1812 and destroyed the remainder of the crop.

Western Lake Erie and the surrounding land areas on Ohio, Michigan and Canadian Ontario were the scenes of skirmishes and battles during the War of 1812. The American cause suffered a series of humiliating defeats at the outset of the struggle. General William Hull's invasion of Canada failed, and Hull, in disgrace, surrendered Detroit to the British in August 1812. The force under General James Winchester was annihilated at the River Raisin (Monroe, Michigan), in January 1813. British and Indian invasions of Ohio at Fort Meigs (Perrysburg) and at Fort Stephenson (Fremont) were repulsed in May and August. The turning point of the war in The Old Northwest came with Oliver Hazard Perry's victory over the British fleet in the Battle of Lake Erie, 10 September 1813. The naval victory made it possible for General William Henry Harrison to invade Canada and defeat the British and Indians at the River Thames in October 1813.

Put-in-Bay Harbor was used by Perry as a base of operations. From the Bass Islands, he could quickly sail to Sandusky Bay for conferences with Harrison or scout the British forces at Fort Malden (Amherstburg, Ontario), in the Detroit River. When the men and ships were not so engaged, there were training duties such as preparing the ships for actions and gunnery practice. The American fleet had sailed from Erie, Pennsylvania, on 12 August 1813 and arrived off Sandusky Bay on the sixteenth. Perry conferred with Generals Harrison and Lewis Cass regarding the next step to take in prosecuting the campaign. The British fleet under Captain Robert H. Barclay was sighted by a lookout in the masthead of Perry's flagship, the brig Lawrence, at 5:00 a.m., Friday, 10 September 1813. The Battle of Lake Erie began at 11:45 a.m. and ended a few minutes after 3:00 p.m. British supremacy on the lake came to an end with the capture of the entire enemy fleet of six vessels. The conflict began eight miles northwest of Put-in-Bay and reached its climax at West Sister Island, fourteen miles away. The triumphant American captain dashed off a short note on the back of an old letter to William Henry Harrison:

> U.S. Brig Niagara, Off Western Sister Island head of Lake Erie, Sept. 10, 1813, 4 p.m. <br><br>
> Dear General --
> We have met the enemy and they are ours; two ships, two brigs, one schooner and one sloop. <br><br>
> Yours with great respect and esteem,<br><br>
> O.H. Perry

After the War of 1812, Aschell (Shell) Johnson lived on Put-in-Bay for three years. The next settlers were Henry and Sally Hyde who came in 1818. The Hydes brought 500 head of sheep to the island. A.P. Edwards then began to develop Put-in-Bay, bringing in laborers to erect the necessary buildings. John Pierpoint built a dock in the harbor and another one known as the West Dock.

The first permanent settler to come to Put-in-Bay was Philip Vroman in 1843. He settled on the island and remained on the island until his death 68 years later. In 1845 Gibraltar Island in the harbor was occupied by a group of government surveyors and engineers who were engaged in making charts of the lake. They found it necessary to cut a strip 45 feet wide running through the woods of Put-in-Bay so they could site the instruments properly. The strip was used as a road by the islanders called "Sight Road". Today it is referred to as the airport road, officially it is Langram Road.

In 1854 a Spanish merchant name Joseph de Rivera bought South Bass, Middle Bass, Sugar, Gibraltar, Ballast and Starve Island for a price of $44,000. He began to develop to islands building a saw mill and a starve mill in the fall of 1854. He had the county engineer survey the area in 10-acre lots. In the first ten years, de Rivera sold 42 parcels of land in South and Middle Bass. He sold a quarter acre of land to the South Bass Board of Education for a dollar. The park downtown is named de Rivera Park in his honor, and a trust is responsible for the park and other land still today. The grape-growing and wine-making industry began in the Lake Erie Island in the 1850s, and Put-in-Bay's attraction as a historical island resort was being developed. Large celebrations were held in 1852, 1858, and 1859 honoring Oliver Hazard Perry's victory over the British in 1813. Put-in-Bay was becoming known for its delicious grapes and excellent wines and as a place where the vacationist, via the steamboat, could "get away from it all" for a few hours. The population grew as farmers came to the island to plant vineyards and as others became involved in the resort business. About 500 persons were permanent residents of Put-in-Bay by the early 1860s.

In 1866 a story in the Sandusky Daily Commercial Register told of the growth of Put-in-Bay township. Islanders owned 103 horses, 165 cattle, 206 hogs and one mule. The fields were planted in wheat, oats, buckwheat, rye, barley, potatoes, sorghum, tobacco, hay and clover. The vineyards were a main source of income. 72 acres of vines had been planted in 1865 to bring the total to 422. Grape production for 1865 totaled 1,117,801 pounds and 33,805 gallons of wine were pressed. The future looked bright for island farmers.

Local island government was now desired and to this end, John Stone, Simon Fox and others from the three Bass Islands petitioned the Ottawa County commissioners for permission to organize Put-in-Bay township. On June 22, 1861, the electors selected their town trustees. In May of 1876, 15 years later after the three islands were organized as a township, a portion of South Bass was incorporated as a village which we now refer to as downtown Put-in-Bay.

St. Paul's Episcopal Church was built in 1865 on land purchased by Jay Cooke from Jose DeRivera for $10.00 (the land for the school was sold for $1.00). The deed to the land stipulated it was for the construction of an Episcopal church. Islanders raised the initial funds to build a church and were financially assisted by Jay Cooke. Jay Cooke's heirs gave the land to the Episcopal church in the early 1900s. Mother of Sorrows Roman Catholic Church was established in 1866. The Put-in-Bay Telegraph Company was incorporated in 1873, with a two and seven-eighth mile cable between Catawba Point and South Bass Island. In the 1930s, dial phones replaced old hand-cranked wall instruments. In May 1906, the street lighting system was converted to electricity.

In its heyday, around the 1850s to the 1900s, several steamships, some holding up to 1,500 passengers, serviced the island on a regular basis. Tourists were treated to a variety of hotels, including 300 x 600 foot Hotel Victory with 625 guest rooms, at that time the largest resort hotel in America featuring the first coed swimming pool. Elaborate ceremonies were planned for the laying of the cornerstone of The Victory. Seven steamboats brought 8,000 people to the island. The Beebe house with a wide hall running 500 feet through the center had a dining room that could seat nearly a thousand diners. The hotel could house over 800 persons. Unfortunately, the Victory Hotel caught fire and burned to the ground before it could be fully utilized.

Put-in-Bay has been a summer resort for more than 100 years. Today, Put-in-Bay is a vibrant tourist resort complete with bars, hotels, boating, fishing, a monument, golf cart rentals, caves and much more. For more information on the history of Put-in-Bay, we suggest you read Isolated Splendor by Robert Dodge where most of the information for this brief history page was obtained. Come visit the South Bass Island, or Put-in-Bay as it is better known, and see for yourself.

Check out more at the [Put-in-Bay](https://www.visitputinbay.com) website.

</article>
