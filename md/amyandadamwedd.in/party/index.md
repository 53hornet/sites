---
pagetitle: Bridal Party
---
<article>

# The Bridal Party

## (We roped them in)

Heads up! If you're a member of the wedding party then you have access to [this super secret page](secret) with information that just you need!

### The Bridesmaids

![Annie Martinez, maid of honor](annie.jpg)

![Maggie Gross](maggie.jpg)

![Julia Franquesa](julia.jpg)

![Alexis Michalos](alexis.jpg)

### The Groomsmen

![Daniel Carpenter, best man, brother of the groom](dan.jpg)

![Michael Pratt](michael.jpg)

![Geoffrey Ringlee](geoff.jpg)

![Ethan Buck](ethan.jpg)

</article>
