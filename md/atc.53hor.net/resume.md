---
pagetitle: Adam T. Carpenter
---

# Adam T. Carpenter

## Summary
Devoted, attentive computer science professional adept at customer relations, team leadership, and critical problem-solving with a strong framework in algorithms, mathematics, and cross-platform programming.

## Contact Information
- Address: 102 Armory Street #218, Newport News, VA
- Phone: Cell: (757) 406-4870 Home: (757) 243-2781
- E-mail: atc@53hor.net
- WWW: atc.53hor.net

## Experience

### Application Developer Trainee | June 2018 - July 2018 | Automatic Data Processing
- Constructed a web-based application for fulfilling I-9 forms
- Worked in a Scrum team of seven in two-week sprints
- Assisted in the construction of an Oracle database, Java API, and AngularJS client
- Made use of technologies such as Git, Spring Boot, QueryDSL, and JUnit

### Technology Support | February 2016 - May 2018 | College of William and Mary
- Responsible for diagnosing and resolving hardware and software issues campus-wide
- Provided assistance for clients in person at help desk and over the phone
- Repaired and replaced faulty hardware in student laptop computers
- Performed software and network configuration on student, faculty, and staff machines

### Software Developer | August 2017 - December 2017 | College of William and Mary
- Expanded upon open-source software for testing hybrid Android applications
- Member of small development team working in Scrum sprints of two weeks
- Implemented new features that used Selendroid to control Android web views
- Worked with Java, SQL, Selendroid (Selenium), GitLab, and the Android SDK
- Explored practices of continuous integration/continuous deployment

### Systems Programmer | January 2017 - May 2017 | College of William and Mary
- Created Tic-Tac-Toe game with multiplayer support over a network
- Constructed client and server ends from scratch in C
- Utilized UNIX socket programming for datagram and INET stream data transmission
- Built a GUI for the game using TCL/TK

## Education

### The College of William and Mary, Williamsburg, Virginia | August 2014 - May 2018
- GPA 3.61
- Dean's list seven out of eight semesters
- Favorite areas of study: systems programming, software engineering
- Favorite class: CSCI 427 Computer Graphics
- Extracurricular project: President (3 years), level designer for the Video Game Design Club at W&M

## Machine Languages
Proficient | Familiar
---: | :---
C | Perl
C++ | Haskell
Java | SQL, PL/SQL
HTML, CSS | TypeScript
Python | 
JavaScript |

## Skills
Soft | Hard
---: | :---
Data structures | Problem-solving skills
Algorithms | Communication
Logical mathematics | Critical thinking
Computer architecture, memory | Leadership skills
Network configuration | Customer relations
Adobe CS | Creativity |
Adobe Premiere | 
Autodesk Maya 2016 | 
Linux server administration | 

## Passions and Hobbies

### Automotive restoration

I fully disassembled, restored, and rebuilt a 1953 Hudson Hornet with my father in our home garage. Together, we rebuilt the engine, restored and painted various parts and components, and reassembled a running, driving classic automobile. It has successfully completed a 3,000 mile road trip from Virginia to Ohio and Chicago without a hitch.

### Linux home server

I built my own home server out of spare PC parts to act as file storage and web hosting. It boasts a ZFS filesystem, Plex media streaming, Nextcloud self-hosted cloud storage, a Duplicati backup destination, and an Apache web server. It is relatively automated and supports automatic filesystem snapshots and unattended system upgrades.

### Retrocomputing

I collect, study, and tinker with vintage/obsolete computers such as the Commodore 64 and Apple II. I repair them, attempt to understand their architectures, and do minor programming on them.

