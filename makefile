MD = $(shell find md/ -type f -name '*.md')
CSS = $(shell find md/ -type f -name '*.css')
JPG = $(shell find md/ -type f -name '*.jpg')
PNG = $(shell find md/ -type f -name '*.png')
PDF = $(shell find md/ -type f -name '*.pdf')
HTML = $(shell find md/ -type f -name '*.html')

CSS_OUT = $(patsubst md/%.css, html/%.css, $(CSS))
JPG_OUT = $(patsubst md/%.jpg, html/%.jpg, $(JPG))
PNG_OUT = $(patsubst md/%.png, html/%.png, $(PNG))
PDF_OUT = $(patsubst md/%.pdf, html/%.pdf, $(PDF))
HTML_OUT = $(patsubst md/%.html, html/%.html, $(HTML))

53HORNET_OUT = $(patsubst md/53hor.net/%.md, html/53hor.net/%.html, $(MD))
ATC_OUT = $(patsubst md/atc.53hor.net/%.md, html/atc.53hor.net/%.html, $(MD))
WED_OUT = $(patsubst md/amyandadamwedd.in/%.md, html/amyandadamwedd.in/%.html, $(MD))
GLASSY_OUT = $(patsubst md/theglassyladies.com/%.md, html/theglassyladies.com/%.html, $(MD))

.PHONY: all
all: css img pdf html 53hor.net atc.53hor.net amyandadamwedd.in theglassyladies.com

.PHONY: html
html: $(HTML_OUT)

html/%.html: md/%.html
	@echo "html: $< -> $@"
	@mkdir -p "$(@D)"
	@cp "$<" "$@"

.PHONY: pdf
pdf: $(PDF_OUT)

html/%.pdf: md/%.pdf
	@echo "pdf: $< -> $@"
	@mkdir -p "$(@D)"
	@cp "$<" "$@"

.PHONY: css
css: $(CSS_OUT)

html/%.css: md/%.css
	@echo "css: $< -> $@"
	@mkdir -p "$(@D)"
	@cp "$<" "$@"

.PHONY: img
img: $(JPG_OUT) $(PNG_OUT)

html/%.jpg: md/%.jpg
	@echo "img: $< -> $@"
	@mkdir -p "$(@D)"
	@convert "$<" -sampling-factor 4:2:0 -strip -quality 85 -interlace JPEG -colorspace RGB -resize 1000x1000\> "$@"

html/%.png: md/%.png
	@echo "img: $< -> $@"
	@mkdir -p "$(@D)"
	@convert "$<" -strip -quality 90 -resize 1000x1000\> "$@"

.PHONY: 53hor.net
53hor.net: $(53HORNET_OUT)

html/53hor.net/%.html: md/53hor.net/%.md md/53hor.net/nav.html
	@echo   "53hor.net: $< -> $@"
	@mkdir -p "$(@D)"
	@pandoc "$<" -f markdown -t html5 -s --toc --toc-depth=2 -H md/53hor.net/background.html -B md/53hor.net/nav.html -c /styles.css -o "$@"

.PHONY: atc.53hor.net
atc.53hor.net: $(ATC_OUT)

html/atc.53hor.net/%.html: md/atc.53hor.net/%.md md/atc.53hor.net/nav.html
	@echo "atc.53hor.net: $< -> $@"
	@mkdir -p "$(@D)"
	@pandoc "$<" -f markdown -t html5 -s --toc --toc-depth=3 -B md/atc.53hor.net/nav.html -c /styles.css -o "$@"

.PHONY: amyandadamwedd.in
amyandadamwedd.in: $(WED_OUT)

html/amyandadamwedd.in/%.html: md/amyandadamwedd.in/%.md md/amyandadamwedd.in/nav.html
	@echo "amyandadamwedd.in: $< -> $@"
	@mkdir -p "$(@D)"
	@pandoc "$<" -f markdown -t html5 -s -B md/amyandadamwedd.in/nav.html -c /styles.css -o "$@"

.PHONY: theglassyladies.com
theglassyladies.com: $(GLASSY_OUT)

html/theglassyladies.com/%.html: md/theglassyladies.com/%.md md/theglassyladies.com/nav.html
	@echo "theglassyladies.com: $< -> $@"
	@mkdir -p "$(@D)"
	@pandoc "$<" -f markdown -t html5 -s -H md/theglassyladies.com/favicon.html -B md/theglassyladies.com/nav.html -c /styles.css -o "$@"

clean:
	@rm -rf html/
